Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }
  devise_scope :user do
    root to: "users/registrations#new"
  end

  get 'welcome/index'
  get '/index' => 'secret_code#index'
  post '/generate' => 'secret_code#generate'

  post "api/v1/gitlabel", to: 'api/v1/git_label#new_label' # Accept github webhook
end
