README
======

### This is the SD2 Labs assignment. User need to implement devise, bootstrap and cancancan gem. Only admin can generate secret codes and access the list of associated secret codes with email.
**Here is the complete detail of assignment:** https://bitbucket.org/gaurav2728/sd2labs/src/master/problem.txt

SOLUTION
========

### Ruby version
> ruby 2.4+

### System dependencies
> rails 5.1.6

### Database
> Postgresql 9+

### Database creation
> rails db:reset

### Deployment
> Heroku Server **Check live application here** http://sd2labs.herokuapp.com/

### Test
> Normal user simply do the registration and automatically unique secret code assign when user complete the registration.

> Admin can generate secret code and also check all secret codes associate with registered users.
> Login - http://sd2labs.herokuapp.com/users/sign_in

**Admin email**
> gaurav2728@gmail.com

**Password**
> 123456
