class SecretCodeController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  def index
    @secret_codes = SecretCode.all
  end

  def generate
    @new_codes = SecretCode.new_codes(permitted_params)
  end

  private
    def permitted_params
      params.require(:secret_code).permit(:numbers) ? params[:secret_code][:numbers].to_i : 10
    end
end
