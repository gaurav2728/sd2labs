require 'net/http'
require 'uri'

class Api::V1::GitLabelController < ApplicationController

  def new_label
    logger.debug(params)
    if params[:action].eql?('new_label')
      uri = URI.parse("https://gaurav.requestcatcher.com/test")
      request = Net::HTTP::Post.new(uri)
      request.body = "Label params[:label][:name] was added to issue params[:issue][:url]"

      req_options = {
        use_ssl: uri.scheme == "https",
      }

      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        http.request(request)
      end
      render json: { success: 'Successfully posted over Slack'  }, status: 200
      logger.debug(response)
    else
      render json: { error: 'Json payload is in wrong format' }, status: 422
    end
  end
end
