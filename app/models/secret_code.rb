class SecretCode < ApplicationRecord
  belongs_to :user, optional: true

  NumberOfCodes = [1, 10, 20, 50, 100]

  validates :code, presence: true, uniqueness: true

  # Generate unique uuid that should not exists in db
  def self.new_codes(n)
    codes = []
    0..n.times do
      code = SecureRandom.uuid
      exists?(code: code) ? codes << SecureRandom.uuid : codes << code
    end
    codes
  end
end
