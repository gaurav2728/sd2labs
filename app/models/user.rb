class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :secret_code, autosave: true, dependent: :destroy

  accepts_nested_attributes_for :secret_code

  def admin?
    role == 'admin'
  end
end
