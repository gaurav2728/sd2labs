class CreateSecretCodes < ActiveRecord::Migration[5.1]
  def change
    create_table :secret_codes do |t|
      t.integer  :user_id, null: true
      t.string   :code,    null: false

      t.timestamps
    end
  end
  add_index :secret_codes, :code, unique: true
end
